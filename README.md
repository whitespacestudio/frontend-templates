# Frontend build template

A starter framework for building front-end website templates in PHP.

### Installing

Copy all files and folders to your local development folder, then update the variables at the top of `build/includes/functions.php`.
The document root is the `build` folder. Any uncompressed/uncompiled files should be stored in the relevant folder in `src`. The idea is that this build is combined with the [CSS style guide](https://bitbucket.org/stephenelford/base-css-template) and ultimately incorporated into the CMS starter build.

```
$site_name = "Name of the website";
$site_url = "http://localhost/websitedir/build/";
$srv_root = "/server/path/to/the/build/directory/";
```

## Authors

* **Steve Elford** - *Initial build and ongoing work*
