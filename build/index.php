<?php
/**
 * Home page template
 *
 * @author Steve Elford
 */
$slug = 'home';
require_once("includes/functions.php");
$pageTitle = 'Home page template - '.SITE_NAME;
include(SRV_ROOT."includes/meta.php");
?>
</head>
<body id="home">

	<?php include(SRV_ROOT."includes/header.php"); ?>
		
	
	<?php include(SRV_ROOT."includes/footer.php"); ?>
