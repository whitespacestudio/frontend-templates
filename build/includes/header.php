<?php
/**
 * The page header.
 * Included on every page. Includes the header element and opens any wrapper elements.
 *
 * @author Steve Elford 
 */
?>
<nav id="mobile_nav">
	<ul class="nav  mainNav level_1">
		<li<?=$slug=='home'?' class="active"':''?>><a href="<?=SITE_URL?>">Home</a></li>
	</ul>
</nav>	
<div id="wrap">

	<div id="container">
	
		<header>
			
			<div class="container">
				
				<div id="site_logo">
					<a href="<?=SITE_URL?>"><img src="<?=SITE_URL?>images/logo.svg" alt="<?=SITE_NAME?>"/></a>
				</div>
								
			</div>

			<nav>
				<div class="container">
					<ul class="mainNav">
						<li<?=$slug=="home"?' class="active"':''?>><a href="<?=SITE_URL?>">Home</a></li>
					</ul>
				</div>									
			</nav>			
						
		</header>
		
		<div id="blur">
			