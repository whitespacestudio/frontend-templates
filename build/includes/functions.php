<?php
/**
 * Global PHP functions and variable declarations.
 * TO be included on every page
 *
 * @author Steve Elford
 */


/* Site name */
$site_name = "Example site";
/* URL of the site homepage. Must end with a forward-slash */
$site_url = "http://".getenv('HTTP_HOST')."/";
/* Full server path to the site root. Must end with a forward-slash */
$srv_root = $_SERVER["DOCUMENT_ROOT"]."/";


define('SITE_NAME', $site_name);
define('SITE_URL', $site_url);
define('SRV_ROOT', $srv_root);

$pageTitle = isset($pageTitle) ? $pageTitle : SITE_NAME;
if(!isset($slug)) $slug = '';
?>